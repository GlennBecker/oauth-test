const { Machine, interpret, assign, send } = XState
const storageKey = 'nonsensica_google_creds'
const app = document.getElementById('app')

function parseCreds(hash) {
  var params = {};
  var regex = /([^&=]+)=([^&]*)/g, m;

  while (m = regex.exec(hash)) {
    params[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
  }
  return params
}

const getCredsFromHash = () => {
  let hash = window.location.hash
  if (!hash) { return null }
  let creds = parseCreds(hash)
  return creds
}

function submit({ endpoint, method, params }) {
  var form = document.createElement('form')
  form.setAttribute('method', method)
  form.setAttribute('action', endpoint)

  for (var p in params) {
    var input = document.createElement('input')
    input.setAttribute('type', 'hidden');
    input.setAttribute('name', p);
    input.setAttribute('value', params[p]);
    form.appendChild(input)
  }

  document.body.appendChild(form);
  form.submit();
}

function oauthSignIn() {
  var oauth2Endpoint = 'https://accounts.google.com/o/oauth2/v2/auth';
  var params = {
    'client_id': '459564294897-72rjdn8hgdriqjif4ttil3pl9q1vmrsp.apps.googleusercontent.com',
    'redirect_uri': 'https://google-oauth-test-issac.netlify.app',
    'response_type': 'token',
    'scope': 'https://www.googleapis.com/auth/userinfo.email',
    'include_granted_scopes': 'true',
    'state': 'pass-through value'
  };
  submit({ endpoint: oauth2Endpoint, method: 'GET', params: params })
}

const setError = (msg) => {
  const errorField = document.getElementById('error-field')
  errorField.innerText = msg
}

const submitForm = (context) => {
  return fetch('/api/update', {
    method: 'POST',
    body: JSON.stringify({
      credentials: context.credentials,
      form: {}
    })
  })
    .catch(error => error)
}

const machine = Machine({
  initial: 'logged-out',
  context: { credentials: undefined },
  states: {
    'logged-out': {
      on: {
        GOT_CREDENTIALS: {
          target: 'logged-in',
          actions: [assign({
            credentials: (ctx, event) => event.credentials
          })]
        },
        LOG_IN: { actions: [() => oauthSignIn()] }
      },
    },
    'logged-in': {
      on: { SUBMIT: 'submitting', LOG_OUT: 'logged-out' }
    },
    'submitting': {
      invoke: {
        src: submitForm,
        onDone: 'success',
        onError: { target: 'error', actions: (ctx, event) => setError(event.data) },
      }
    },
    'success': {
      on: {
        CLEAR: 'logged-in',
        LOG_OUT: 'logged-out'
      }
    },
    'error': {
      on: {
        RETRY: 'submitting',
        CLEAR: 'logged-in',
        LOG_OUT: 'logged-out'
      }
    }
  }
});

const state = interpret(machine)
  .onTransition(state => {
    console.log(state.value)
    app.setAttribute('data-state', state.value)
  })
  .start()

const form = document.getElementById('form')

const readFormData = () => {
  console.log(form)
}

document.getElementById('login-button')
  .addEventListener('click', () => state.send('LOG_IN'))

document.getElementById('logout-button')
  .addEventListener('click', () => state.send('LOG_OUT'))

document.querySelectorAll('.submit-form-button')
  .forEach(button => button.addEventListener('click', (e) => {
    e.preventDefault()
    state.send('SUBMIT')
  })
  )

document.querySelectorAll('.clear-button')
  .forEach(button => button.addEventListener('click', (e) => {
    e.preventDefault()
    state.send('CLEAR')
  }))

const getCreds = () => {
  let hashCreds = getCredsFromHash()
  if (hashCreds) {
    state.send({ type: 'GOT_CREDENTIALS', credentials: hashCreds })
    history.replaceState(null, '', ' ');
    return;
  }
}

getCreds()